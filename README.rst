AGENDA. Luis D�ez
==================

Instalaci�n
------------

Se accede a la p�gina **https://bitbucket.org/luisdiez/agendaphpdwes** y se descarga la carpeta.

Una vez descargada se cambia el nombre de la carpeta y se le llama ``agenda``.

Se accede a la aplicaci�n desde localhost/agenda una que se ubica en el servidor propio y habiendo cambiado los permisos de la carpeta


Funcionamiento
------------------

Lo primero que hay que hacer es crear la base de datos. Si ya existe el archivo avisar�. En el index puedes elegir en tre acceder con tu usuario y contrase�a o dar de elta un nuevo usuario.

Al crear la base de datos se crean 2 tablas: Contactos y usuarios. Los usuarios predeterminados son user1 y user2 con id�nticas passwords. 

Una vez que se accede a la aplicaci�n ya se puede manejar la agenda.

Cada usuario tendr� sus propios contactos y s�lo tendr� acceso a ellos si se identifica

Existe un archivo php oculto al que el cliente no tendr� acceso donde se pueden ver los usuarios y sus claves cifradas. 

localhost/agenda/listado-usuarios.php


