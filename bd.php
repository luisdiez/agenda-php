<?php 
	include ('base.php'); 
	echo $cabecera;
?>
<?php
	// variables
	
	// Tamaño de los campos en la tabla
	define("TAM_NOMBRE",     40);  // Tamaño del campo Nombre
	define("TAM_APELLIDOS",  60);  // Tamaño del campo Apellidos
	define("TAM_TELEFONO",   10);  // Tamaño del campo Teléfono
	define("TAM_EMAIL",     50);  // Tamaño del campo Correo
	define("TAM_LOGIN",     10);
	define("TAM_PASSWORD",     15);
	
	// Consulta de creación de tabla en SQLite
	$crear_tabla = "CREATE TABLE IF NOT EXISTS agenda (
	    id INTEGER PRIMARY KEY AUTOINCREMENT,
		id_usuario,
	    nombre VARCHAR(" . TAM_NOMBRE . "),
	    apellidos VARCHAR(" . TAM_APELLIDOS . "), 
	    telefono VARCHAR(" . TAM_TELEFONO . "),
	    email VARCHAR(" . TAM_EMAIL. ")
	    )";
	
	$datos= array(
				  array("id_usuario"=> 1,
				  		"nombre"=>"Luis",
				  		"apellidos"=>"Díez",
						"telefono"=>"111223344",
						"email"=>"ld@ld.com"),
				 array("id_usuario"=> 2,
				  		"nombre"=>"Adrian",
				  		"apellidos"=>"Fernandez",
						"telefono"=>"555555555",
						"email"=>"af@af.com"),
			);
	
	// Consulta de creación de tabla en SQLite
	$crear_tabla_usuarios = "CREATE TABLE IF NOT EXISTS usuarios (
	    id INTEGER PRIMARY KEY AUTOINCREMENT,
	    login VARCHAR(" . TAM_LOGIN . "),
	    password VARCHAR(" . TAM_PASSWORD . ")
	    )";
	$datos_usuarios= array(
			array("login"=>"user1",
				  "password"=>"user1"),
			array("login"=>"user2",
					"password"=>"user2")
						
	);
?>
<?php
	// test de acceso a sqlite

	$fichero = 'agenda.db';
	if (file_exists($fichero)) {
	    echo "<h1>EL FICHERO YA EXISTE</h1><br>";
	}else{
		try{
			// crear bases de datos
			$conn=new PDO('sqlite:agenda.db');
			//$conn=new PDO('sqlite::memory:');  --> crea base de datos en memoria no en el sistema de ficheros
			
			$conn->exec($crear_tabla);
			//insertar datos
			//preparar sentencia de insercion
			$insertar="INSERT into agenda(id_usuario,nombre,apellidos,telefono,email)
						VALUES(:id_usuario,:nombre,:apellidos,:telefono,:email)";
			$sentencia=$conn->prepare($insertar);
			foreach($datos as $ag){
				$sentencia->execute($ag);
			}
			echo "TABLA AGENDA CREADA CORRECTAMENTE <br/>";
			$conn->exec($crear_tabla_usuarios);
			//insertar datos
			//preparar sentencia de insercion
			
			
			foreach($datos_usuarios as $ag){
				/*
				$insertar='INSERT into usuarios(login,password)
						VALUES("'.$ag['login'].'","'.$ag['password'].'")';
				$sentencia=$conn->prepare($insertar);
				//echo $insertar.'<br />';
				$sentencia->execute($ag);
				*/
				
				$insertar='INSERT into usuarios(login,password)
						VALUES("'.$ag['login'].'","'.crypt($ag['password']).'")';
				$resultado=$conn->query($insertar);
			}
			
			echo "TABLA USUARIOS CREADA CORRECTAMENTE";
		}catch(PDOException $e){
		echo $e->getMessage();
		}
	}
		//cierra conexion
		$conn=null;
?>
<?php 
	include ('base.php'); 
	echo $pie;
?>
