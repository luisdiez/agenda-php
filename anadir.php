<?php 
include ('base.php'); 
include_once('comprobar_session.php');
echo $cabecera;
	
$id_usuario_loggeado = $_SESSION['id_usuario'];
	
?>

<?php
	if (isset($_POST)){
		$nombre=$_POST["nombre"];
		$apellidos=$_POST["apellidos"];
		$telefono=$_POST["telefono"];
		$email=$_POST["email"];
		
		try{
			$conn = new PDO('sqlite:agenda.db');	
			$insertar="INSERT into agenda(id_usuario,nombre,apellidos,telefono,email)
						VALUES(:id_usuario,:nombre,:apellidos,:telefono,:email)";
			$sentencia=$conn->prepare($insertar);
			$sentencia->bindParam(':id_usuario', $id_usuario_loggeado);
			$sentencia->bindParam(':nombre', $nombre);
			$sentencia->bindParam(':apellidos', $apellidos);
			$sentencia->bindParam(':telefono', $telefono);
			$sentencia->bindParam(':email', $email);
			$sentencia->execute();
			 
			 /* $insertar='INSERT INTO agenda (id_usuario,nombre,apellidos,telefono,email) 
			 VALUES("'.$id_usuario_loggeado.'","'.$nombre.'","'.$apellidos.'","'.$telefono.'","'.$email.'")';
			 $sentencia=$conn->prepare($insertar);
			 $ok=$sentencia->execute();
			 if (!$ok) die ("No se puede ejecutar sentencia");
			*/
		}catch(PDOException $e){
		echo $e->getMessage();
		} 
		$conn=null;
		header("Location: listado.php");
	}
?>

?>

<?php 
	include ('base.php'); 
	echo $pie;
?>